import { ToClassOptions } from "./typing";

export const isNull = (val: any) => val === null;

export const isUndefined = (val: any) => val === undefined;

export const isNullOrUndefined = (val: any) => isNull(val) || isUndefined(val);

export const isArray = (val: any) => Array.isArray(val);

export const isObject = (val: any) => isNull(val) ? false : val.constructor === Object;

export const isObjectOrArray = (val: any) => isObject(val) || isArray(val);

export const firstLetterCap = (val: string): string => {
  let splitArr: string[] = val.split("_")
  return splitArr.map(v => v.charAt(0).toUpperCase() + v.slice(1)).join("");
}

export const setDefaultNull = (options: ToClassOptions) => options?.nullAlias ? options?.nullAlias : "string"
export const setDefaultNeedPropertyValue = (options: ToClassOptions, key: string) => {
  options.needProperty = options.needProperty || false  
  return options?.needProperty 
   ? options?.interfaceModel 
     ? null
     : options?.needPropertyValue 
       ?`\t\n@property("${key}")`
       :`@property()`
   : null
}


export const setInterfaceModel = (options: ToClassOptions) => options?.interfaceModel ? 'export interface' : `export class`

export const setKeyNotNull = (options: ToClassOptions, key: string) => `${key}${options?.interfaceModel ? '' : '!'}` 

export const stripIndent = (template: TemplateStringsArray, ...expressions: any[]) => {
  let result = template.reduce((prev, next, i) => {
    let expression = expressions[i - 1];

    if (isArray(expression)) {
      expression = expression.join('');
    }
    return prev + expression + next;
  });

  const match = result.match(/^[^\S\n]*(?=\S)/gm);
  const indent = match && Math.min(...match.map(el => el.length));

  if (indent) {
    const regexp = new RegExp(`^.{${indent}}`, 'gm');
    result = result.replace(regexp, '');
  }

  result = result.replace(/^[^\S\n]+/gm, ' ');
  result = result.trim();

  return result;
}




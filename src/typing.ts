import { PathLike } from "fs";

export type JosnType = { [key: string]: any };

export type Config = {
  saveTypeFilePath: PathLike
};

export interface ToClassOptions {
  // 把 null 处理为何种类型
  nullAlias?: string;

  // 是否需要自动生成 Property 装饰器，默认为 false（不需要）
  needProperty?: boolean;

  // 是否需要自动生成Property装饰器的 默数值，默认为 false（不需要）
  needPropertyValue?: boolean;

  // 是否需要生成 interface 模式，默认: false，不生成interface，true 生成 interface 
  interfaceModel?: boolean
}
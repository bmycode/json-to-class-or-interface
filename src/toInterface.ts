import { toClass } from "./toClass"
import { JosnType, ToClassOptions } from "./typing"

export const toInterface = (className: string = "JsonToInterface", rawJson: JosnType | string, options: ToClassOptions = {}): string => {
  options.interfaceModel = true
  return toClass(className, rawJson, options)
}
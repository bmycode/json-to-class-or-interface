# JsonToClassOrInterface

## 状态
 
持续开发中...

- 【完成】~~1: 将开发桌面端软件 和 web网页 进而 方便作为 独立的 工具来使用。~~
- 【完成】~~2: 增加功能，将生成的 Interface 和 class 自动生成 xxx.ts 文件，并保存到你给的项目路径中，方便开发者将本插件和项目请求进行深度集成~~

**基于本插件，自己开发的支持MAC，Linux，Windows 的桌面端项目地址：**
> [https://gitee.com/bmycode/json-class-desktop](https://gitee.com/bmycode/json-class-desktop)

## 前言
顾名思义，这是一个把前端接口的`json`数据转化为 `typescript` 的`Interface`或`Class`的插件。

当你在用`typescript`开发项目，你是否烦恼过后端接口给我们一坨`json`数据的时候，我们需要为数据 **手动编写大量** 的`Interface`或`Class`，来为请求中间层的方法标注返回类型，然后才能在页面中使用并获得优雅的类型提示

**手动写...很麻烦的，为何不 根据 json 数据自动生成？？？！！！**

受到 [flutter](https://flutter.dev/) 的 [json to dart](https://javiercbk.github.io/json_to_dart/) 启发，进而开发本插件。


## 注意

**本插件只能将json自动生成为 `Interface` 或 `Class`，避免我们手动编写的繁杂过程，但并不能实现类似 flutter json_serializable 将 json数据和类进行映射（序列化）的功能！！**


如果你想实现全套 json_serializable 的功能，可以在调用 `toClass` 方法的时候将参数 `needProperty` 开启并设置为`true`（参数说明看下面）。

这时候将生成 `json-mapper-class` 插件所需要的 **序列化代码**

---

至于为什么需要 `json-mapper-class` 这个插件，相信开发过`flutter`的你应该很清楚，不清楚去看官网说的说明：

> [https://docs.flutter.dev/data-and-backend/serialization/json](https://docs.flutter.dev/data-and-backend/serialization/json)

---

如果你需要使用`json和类映射`，那么要在项目里下载引入本插件才能使用，插件地址：

> [json-mapper-class](https://www.npmjs.com/package/json-mapper-class)

---

可能你会疑惑甚至一脸懵逼，我知道你很急，但是你先别着急，看说明你思考下就明白了：

插件有两个：
- json-class-interface
- json-mapper-class

它们之间的使用关系举例说明如下：

 - 假设 http://www.xxx.com/newsInfo 接口返回了 **新闻详情数据**
 - 这时我们 使用插件 `json-class-interface` 将接口返回数据转为 类(needProperty参数需要开启)，然后将 插件返回的代码复制保存到 xxx.ts 中。
 - 最后 使用插件 `json-mapper-class` 将上一步的类和接口json传入，本插件会自动序列化json存储到类中，**请求中间层方法返回数据类型和页面上直接 使用 类 即可**

 **当然你也可以把这两个插件和你的项目进行深度的集成，让它们更加`自动化`,`无感`,`便捷`,`高效`。**

## 安装

> npm i --save json-class-interface

## 使用案例

```typescript
import { toClass, toInterface } from "json-class-interface";

// Json 数据
let json1 = {
  status: 200,
  message: "请求成功",
  result_list: [
    { 
      id: 1, 
      name: "新闻1", 
      tags_arr: [ 
        { 
          tags_id: 1, 
          tags_title: '标签标题1' 
        }, 
        { 
          tags_id: 1, 
          tags_title: '标签标题1' 
        } 
      ] 
    },
    { 
      id: 2, 
      name: "新闻2", 
      tags_arr: [ 
        { 
          tags_id: 1, 
          tags_title: '标签标题2' 
        }, 
        { 
          tags_id: 1, 
          tags_title: '标签标题2' 
        } 
      ] 
    },
  ]
}

// 1 转换为 Class

// JsonToClass: 第一层类的名称，随便自定义
let type: string = toClass("JsonToClass", json1)
console.log(type);

// json转换为class的结果输出：
// export class TagsArr {
//  tags_id!: number;
//  tags_title!: string;
// } 

// export class ResultList {
//  id!: number;
//  name!: string;
//  tags_arr!: TagsArr[];
// } 

// export class JsonToClass {
//  status!: number;
//  message!: string;
//  result_list!: ResultList[];
// } 


// 2 转化为 Interface

let Interface: string = toInterface("JsonToInterface", json1)
console.log(Interface);

// json转换为Interface的结果输出：
// export interface TagsArr {
//  tags_id: number;
//  tags_title: string;
// } 

// export interface ResultList {
//  id: number;
//  name: string;
//  tags_arr: TagsArr[];
// } 

// export interface JsonToInterface {
//  status: number;
//  message: string;
//  result_list: ResultList[];
// } 
```

# 方法 


将 `json` 数据转化为 `class`

## toClass(className: string = "JsonToClass", rawJson: JosnType | string, options: ToClassOptions = {}): string

---

将 `json` 数据转化为 `interface`，本方法的效果 和 `toClass` 参数 `interfaceModel: true` 效果一样。为了方便使用，故提供`toInterface`方法

## toInterface(className: string = "JsonToInterface", rawJson: JosnType | string, options: ToClassOptions = {}): string

---

方法参数说明：

| 名称 | 类型 | 默认值 |
| :-----:| :----: | :----: |
| className | string | JsonToClass 或 JsonToInterface |
| rawJson | JosnType 或 string | 不为空 |
| options | ToClassOptions | {} |


**JosnType 类型如下：**
```
export type JosnType = { [key: string]: any };
```

**ToClassOptions 类型如下：**
```
export interface ToClassOptions {
  nullAlias?: string;
  needProperty?: boolean;
  needPropertyValue?: boolean;
  interfaceModel?: boolean
}
```

|  名称   | 类型  | 默认值  | 说明  |
|  :----:| :----:|:----:|:----:| 
| nullAlias  | string | "string" | 如果对象值为null，转化为 class 或 interface 时候，给这个对象key个默认类型  | 
| needProperty  | boolean | false | 是否需要生成Property装饰器，用到插件 json-mapper-class 才需要开启 | 
| needPropertyValue  | boolean| false  | 是否需要生成Property装饰器的默认值，用到插件 json-mapper-class 才需要开启| 
| interfaceModel  | boolean| false  | 是否需要生成 interface 模式  | 



import { JosnType, ToClassOptions } from "./typing";
export declare const toClass: (className: string, rawJson: JosnType | string, options?: ToClassOptions, repeat?: boolean) => string;

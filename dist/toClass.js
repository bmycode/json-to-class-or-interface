"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toClass = void 0;
const utils_1 = require("./utils");
let TextResult = "";
const toClass = (className = "JsonToClass", rawJson, options = {}, repeat = false) => {
    typeof (rawJson) === "string" ? rawJson = JSON.parse(rawJson) : null;
    (0, utils_1.isArray)(rawJson) ? rawJson = rawJson[0] : null;
    if (!repeat) {
        TextResult = "";
    }
    let classText = (0, utils_1.stripIndent) `
    ${(0, utils_1.setInterfaceModel)(options)} ${className} {
      ${Object.keys(rawJson).map(key => {
        let ArrayInsideNotObject = false;
        if ((0, utils_1.isObjectOrArray)(rawJson[key])) {
            if ((0, utils_1.isArray)(rawJson[key]) && (0, utils_1.isObject)(rawJson[key][0])) {
                (0, exports.toClass)((0, utils_1.firstLetterCap)(key), rawJson[key], options, true);
            }
            if ((0, utils_1.isObject)(rawJson[key])) {
                ArrayInsideNotObject = false;
                (0, exports.toClass)((0, utils_1.firstLetterCap)(key), rawJson[key], options, true);
            }
        }
        return `${(0, utils_1.setDefaultNeedPropertyValue)(options, key) == null ? '' : (0, utils_1.setDefaultNeedPropertyValue)(options, key)}
            ${(0, utils_1.setKeyNotNull)(options, key)}: ${ArrayInsideNotObject
            ? `${typeof rawJson[key][0]}[]`
            : (0, utils_1.isObjectOrArray)(rawJson[key])
                ? `${(0, utils_1.firstLetterCap)(key)}${(0, utils_1.isArray)(rawJson[key]) ? '[]' : ''}`
                : (0, utils_1.isNull)(rawJson[key]) ? (0, utils_1.setDefaultNull)(options) : typeof rawJson[key]};
          `;
    })}
    }
  `;
    TextResult += `${classText} \n\n`;
    return TextResult;
};
exports.toClass = toClass;

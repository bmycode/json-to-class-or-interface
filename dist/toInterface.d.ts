import { JosnType, ToClassOptions } from "./typing";
export declare const toInterface: (className: string, rawJson: JosnType | string, options?: ToClassOptions) => string;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stripIndent = exports.setKeyNotNull = exports.setInterfaceModel = exports.setDefaultNeedPropertyValue = exports.setDefaultNull = exports.firstLetterCap = exports.isObjectOrArray = exports.isObject = exports.isArray = exports.isNullOrUndefined = exports.isUndefined = exports.isNull = void 0;
const isNull = (val) => val === null;
exports.isNull = isNull;
const isUndefined = (val) => val === undefined;
exports.isUndefined = isUndefined;
const isNullOrUndefined = (val) => (0, exports.isNull)(val) || (0, exports.isUndefined)(val);
exports.isNullOrUndefined = isNullOrUndefined;
const isArray = (val) => Array.isArray(val);
exports.isArray = isArray;
const isObject = (val) => (0, exports.isNull)(val) ? false : val.constructor === Object;
exports.isObject = isObject;
const isObjectOrArray = (val) => (0, exports.isObject)(val) || (0, exports.isArray)(val);
exports.isObjectOrArray = isObjectOrArray;
const firstLetterCap = (val) => {
    let splitArr = val.split("_");
    return splitArr.map(v => v.charAt(0).toUpperCase() + v.slice(1)).join("");
};
exports.firstLetterCap = firstLetterCap;
const setDefaultNull = (options) => options?.nullAlias ? options?.nullAlias : "string";
exports.setDefaultNull = setDefaultNull;
const setDefaultNeedPropertyValue = (options, key) => {
    options.needProperty = options.needProperty || false;
    return options?.needProperty
        ? options?.interfaceModel
            ? null
            : options?.needPropertyValue
                ? `\t\n@property("${key}")`
                : `@property()`
        : null;
};
exports.setDefaultNeedPropertyValue = setDefaultNeedPropertyValue;
const setInterfaceModel = (options) => options?.interfaceModel ? 'export interface' : `export class`;
exports.setInterfaceModel = setInterfaceModel;
const setKeyNotNull = (options, key) => `${key}${options?.interfaceModel ? '' : '!'}`;
exports.setKeyNotNull = setKeyNotNull;
const stripIndent = (template, ...expressions) => {
    let result = template.reduce((prev, next, i) => {
        let expression = expressions[i - 1];
        if ((0, exports.isArray)(expression)) {
            expression = expression.join('');
        }
        return prev + expression + next;
    });
    const match = result.match(/^[^\S\n]*(?=\S)/gm);
    const indent = match && Math.min(...match.map(el => el.length));
    if (indent) {
        const regexp = new RegExp(`^.{${indent}}`, 'gm');
        result = result.replace(regexp, '');
    }
    result = result.replace(/^[^\S\n]+/gm, ' ');
    result = result.trim();
    return result;
};
exports.stripIndent = stripIndent;

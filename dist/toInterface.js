"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toInterface = void 0;
const toClass_1 = require("./toClass");
const toInterface = (className = "JsonToInterface", rawJson, options = {}) => {
    options.interfaceModel = true;
    return (0, toClass_1.toClass)(className, rawJson, options);
};
exports.toInterface = toInterface;

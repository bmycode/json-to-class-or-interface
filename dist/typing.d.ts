/// <reference types="node" />
import { PathLike } from "fs";
export declare type JosnType = {
    [key: string]: any;
};
export declare type Config = {
    saveTypeFilePath: PathLike;
};
export interface ToClassOptions {
    nullAlias?: string;
    needProperty?: boolean;
    needPropertyValue?: boolean;
    interfaceModel?: boolean;
}

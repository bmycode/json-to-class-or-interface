import { toClass, toInterface } from "./src";

let json1 = {
  status: 200,
  message: "请求成功",
  result_list: [
    { id: 1, name: "新闻1", tags_arr: [{ tags_id: 1, tags_title: '标签标题1' }, { tags_id: 1, tags_title: '标签标题1' }] },
    { id: 2, name: "新闻2", tags_arr: [{ tags_id: 1, tags_title: '标签标题2' }, { tags_id: 1, tags_title: '标签标题2' }] },
  ]
}

let json2 = [
  { id: 1, name: "新闻1" },
  { id: 2, name: "新闻2" },
]

let json3 = {
  "code": 200,
  "message": null,
  "data": {
    "next_page": 0,
    "result": [
      {
        "opus_id": 2,
        "user_id": 1,
        "opus_title": "测试作品2",
        "opus_introduce": "不知道写啥介绍了2",
        "opus_time": "2022-06-29 21:46:00",
        "opus_satisfied": 2,
        "opus_see": 1
      },
      {
        "opus_id": 3,
        "user_id": 1,
        "opus_title": "2222",
        "opus_introduce": "不知道写啥介绍了2",
        "opus_time": "2022-06-29 21:45:15",
        "opus_satisfied": 0,
        "opus_see": 0
      }
    ],
    "total_page": 1,
    "current_page": 1
  }
}

let json4 = {
  "id": 1,
  "username": null,
  "password": "19bf89cc955e75616244e1995e649982",
  "list": {
    "id": 1,
    "name": 2
  }
}

let type = toClass("JsonModel", json4, {
  nullAlias: "string",
  // //传 true 可转化为 interface
  // interfaceModel: true,
  needPropertyValue: true,
  needProperty: true
})

console.log(type);


// let type2 = toClass("JsonModel", json1, {
//   // nullAlias: "string",
//   // //传 true 可转化为 interface
//   // interfaceModel: true,
//   // needPropertyValue: true,
//   // needProperty: true
// })

// console.log(type);


// 或者直接调用 toInterface，不需要传interfaceModel参数
// let Interface = toInterface("JsonModel", json1)
// console.log(Interface);


